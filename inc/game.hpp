#pragma once
#include <SFML/Graphics.hpp>
#include <sstream>
using namespace sf;

namespace mt
{
	struct Line
	{
		float x = 0, y = 0, z = 0;
		float X = 0, Y = 0, W = 0;
		float curve = 0, spriteX = 0, clip = 0, scale = 0;
		Sprite sprite;
		int width = 1024;
		int height = 768;
		int roadW = 2000;
		int segL = 200;
		float camD = 0.84;

		Line()
		{
			spriteX = curve = x = y = z = 0;
		}

		void project(int camX, int camY, int camZ)
		{
			scale = camD / (z - camZ);
			X = (1 + scale * (x - camX)) * width / 2;
			Y = (1 - scale * (y - camY)) * height / 2;
			W = scale * roadW * width / 2;
		}

		void drawSprite(RenderWindow& app)
		{
			Sprite s = sprite;
			int w = s.getTextureRect().width;
			int h = s.getTextureRect().height;

			float destX = X + scale * spriteX * width / 2;
			float destY = Y + 4;
			float destW = w * W / 266;
			float destH = h * W / 266;

			destX += destW * spriteX;
			destY += destH * (-1);

			float clipH = destY + destH - clip;
			if (clipH < 0) clipH = 0;

			if (clipH >= destH) return;
			s.setTextureRect(IntRect(0, 0, w, h - h * clipH / destH));
			s.setScale(destW / w, destH / h);
			s.setPosition(destX, destY);
			app.draw(s);
		}
	};

#define MAX_NUMBER_OF_ITEMS 2
	class Menu
	{
	public:
		Menu();
		~Menu();
		void MoveUp();
		void MoveDown();
		void draw(sf::RenderWindow& window);
		int GetPressedItem() { return selectedItemIndex; }

	private:
		int selectedItemIndex;
		sf::Font font;
		sf::Text menu[MAX_NUMBER_OF_ITEMS];

	};
	bool startGame();
	void drawRoad(RenderWindow& w, Color c, int x1, int y1, int w1, int x2, int y2, int w2);
}