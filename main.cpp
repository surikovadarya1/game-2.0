﻿#include <SFML/Graphics.hpp>
#include <game.hpp>
#include <time.h>
#include <iostream>
#include <sstream>
using namespace sf;


int main()
{
	sf::RenderWindow window(sf::VideoMode(1024, 768), "Main menu!");


	sf::Image icon;

	if (!icon.loadFromFile("images/icon.png"))

	{

		return -1;

	}

	window.setIcon(32, 32, icon.getPixelsPtr());


	mt::Menu menu;


	while (window.isOpen())

	{

		sf::Event event;


		while (window.pollEvent(event))

		{

			switch (event.type)

			{

			case sf::Event::KeyReleased:

				switch (event.key.code)

				{

				case sf::Keyboard::Up:

					menu.MoveUp();

					break;


				case sf::Keyboard::Down:

					menu.MoveDown();

					break;


				case sf::Keyboard::Return:

					switch (menu.GetPressedItem())

					{

					case 0:

						window.close();

						mt::startGame();

						break;

					case 1:

						window.close();

						break;

					}


					break;

				}


				break;

			case sf::Event::Closed:

				window.close();


				break;


			}

		}


		window.clear();


		menu.draw(window);


		window.display();

	}

}